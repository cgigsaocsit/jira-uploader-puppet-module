define jira_uploader::client ($jira_base_path, $jira_username, $jira_password = undef, $user = $title, $group, $home_path = '/home') {
        include jira_uploader::params

        concat { "${home_path}/${user}/${$jira_uploader::params::jira_uploader_yml}":
                owner   => $user,
                group   => $group,
                mode    => 640,
        }

        concat::fragment { "jira_uploader::client_${home_path}_${user}_header_comment":
                target  => "${home_path}/${user}/${$jira_uploader::params::jira_uploader_yml}",
                content => template('util/header.erb'),
                order   => '01',
        }

        concat::fragment { "jira_uploader::client_${home_path}_${user}_header":
                target  => "${home_path}/${user}/${$jira_uploader::params::jira_uploader_yml}",
                content => template('jira_uploader/client/jira_uploader_yml_header.erb'),
                order   => '02',
        }

        if $password {
                concat::fragment { "jira_uploader::client_${home_path}_${user}_password":
                        target  => "${home_path}/${user}/${$jira_uploader::params::jira_uploader_yml}",
                        content => template('jira_uploader/client/jira_uploader_yml_password.erb'),
                        order   => '03',
                }
        }
}
