class jira_uploader {
	include jira_uploader::params

	$jira_uploader_rb_path 	= $jira_uploader::params::jira_uploader_rb_path
	$juploader_path		= $jira_uploader::params::juploader_path
	$jira_uploader_yml	= $jira_uploader::params::jira_uploader_yml

        file { $jira_uploader_rb_path:
                ensure  => present,
                owner   => 'root',
                group   => 'root',
                mode    => 755,
                source  => 'puppet:///modules/jira_uploader/jira-uploader/jira_uploader.rb',
        }

	file { $juploader_path:
                ensure  => present,
                owner   => 'root',
                group   => 'root',
                mode    => 755,
                content => template('util/header.erb',
                                'jira_uploader/jira_uploader.sh.erb'),
        }

	file { '/etc/profile.d/juploader.sh':
		ensure	=> present,
		owner   => 'root',
                group   => 'root',
                mode    => 755,
		content => template('util/header.erb',
                                'jira_uploader/juploader_profile.sh.erb'),
	}
}
