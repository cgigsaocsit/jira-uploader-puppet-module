Jira Uploader
================================
A Puppet module for deploying and configuring the Jira Uploader script.

#### Table of Contents
* [Files](#markdown-header-files)

* [Installation](#markdown-header-installation)

* [Usage](#markdown-header-usage)

	* [Deploy the Script](#markdown-header-deploy-the-script)

	* [Deploy User jira_uploader File](#markdown-header-deploy-user-jira_uploader-file)

- - -


Files
================================
This Puppet module deploys the following files.

* jira_uploader.rb: The main Ruby script.

* juploader.sh: A "shortcut" script for performing the upload using the users ~/.jira_uploader.yml file.

* juploader.sh profile.d script: Creates a bash alias for running juploader easier ($ juploader)


Installation
================================
In order for the module to function correctly you must clone the lastest version of the Jira Uploader. To do this simply run the following command under the root of this module.

```
#!shell

$ git submodule init
Submodule 'files/jira-uploader' (https://bitbucket.org/cgigsaocsit/jira-uploader) registered for path 'files/jira-uploader'
```

Usage
================================
### Deploy the Script ###
Simply include the Class resource below in your Puppet manifest. 

This will deploy:

* jira_uploader.rb script to /scripts/jira_uploader.rb

* juploader.sh shell script to /scripts/juploader.sh

* The profile.d script to /scripts/juploader.sh

```
#!puppet

class { 'jira_uploader': }
```

### Deploy User jira_uploader File ###
Simply include the jira_uploader::client defined type below in your Puppet manifest configuring as approperate.

```
#!puppet

jira_uploader::client { 'bherrin6':					# REQUIRED: The user to deploy for
	group           => 'bherrin6',					# REQUIRED: The group that should own the resulting .jira_uploader.yml file
	jira_base_path  => 'https://cm-jira.usa.gov',	# REQUIRED: The jira_base_path
	jira_username   => 'bherring',					# REQUIRED: The users Jira username
	jira_password	=> 'PASSWORD',					# OPTIONAL: The users Jira password (leave this blank and their password will not be included in the resulting .jira_uploader.yml file)
	home_path		=> '/home',						# OPTIONAL: Set the base path for users home directories
}
```
